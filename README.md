# Software Studio 2018 Spring Assignment 01 Web Canvas

## Web Canvas
<img src="example.jpg" height="400px"></img>

## 畫圖工具

可以畫在畫布上的有7個（背景是圓形的）
*   畫筆
*   橡皮擦
*   圓形
*   矩形
*   三角形
*   文字
*   圖片

當按住或是hover住時，有些會有選項可以選，像是調整大小之類的。圖片是選取後直接托曳，就畫的出來了。

## 選項
用來操作的有5種
*   顏色
*   回覆
*   取消回覆
*   清空
*   下載

## bug
*   用firefox時，cursor無法正常顯示