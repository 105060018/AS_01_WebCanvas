const UPDATE_RATE = 100;


$( document ).ready(function() {
    console.log("setup...");
    
    // setting initial stroke 
    var src = "./src/" + stroke.type + "_click.svg";
    $("#"+stroke.type).attr("src",src);
    
    // ===================== setting tool box ================
    $("#brush-size").slider({
        change: function(){
            var size = $("#brush-size").slider("value");
            $("#brush-option label").html("size: " + size);
        },
        create: function(){
            var size = $("#brush-size").slider("value");
            $("#brush-option label").html("size: " + size);
        },
        value : 2,
        max : 20
    });
    $("#eraser-size").slider({
        change: function(){
            var size = $("#eraser-size").slider("value");
            $("#eraser-option label").html("size: " + size);
        },
        create: function(){
            var size = $("#eraser-size").slider("value");
            $("#eraser-option label").html("size: " + size);
        },
        value : 10,
        max : 20
    });

    $("#color").spectrum({
        color: "#000000",
        change: function(color){
            stroke.color = color.toHexString();
        }
    });
    
    $(".toolbox option").each(function(index){
        $(this).css("font-family",$(this).html());
    });

    // start to draw
    setInterval("draw()",UPDATE_RATE);

    // ========== resize canvas =========
    $("canvas").height( $(window).height() - 170);
    $("canvas").attr("height",$(window).height() - 170);
    $("canvas").attr("width",$("canvas").width());
});
$( window ).resize(function(){
    $("canvas").height( $(window).height() - 170);
    $("canvas").attr("height",$(window).height() - 170);
    $("canvas").attr("width",$("canvas").width());
});

// ============== display on canvas ============
var draw = function(){
    stroke.update();
    display.ctx.clearRect(0,0,canvas.width,canvas.height);
    display.stack.forEach(function(shape){
        shape.show();
    });
}


// ============== cursor ==============
var cursor = new Cursor();
display.stack.push(cursor);
$("canvas").hover(function(){
    cursor.display();
},function(){
    cursor.hide();
});

// ============== change tool ============
$(".toolbox>img").click(function(){
    // remove click on stroke.type
    var src = "./src/" + stroke.type + ".svg";
    $("#"+stroke.type).attr("src",src);

    // change stroke type
    stroke.type = $(this).attr("id");
    src = "./src/" + stroke.type + "_click.svg";
    $(this).attr("src",src);
    if(stroke.type==="brush" || stroke.type==="eraser" || stroke.type==="text" || stroke.type==="picture"){
        var optionName = "#" + stroke.type + "-option";
        $(optionName).show();
    }
});

// ============== tool option =============
var brushPopper = new Popper($("#brush"),$("#brush-option"),{
    placement : "bottom"
});
$("#brush").hover(function(){
        if(stroke.type==="brush"){
            $("#brush-option").show();
        }
    },function(){
        $("#brush-option").hide();
});
$("#brush-option").hover(function () {
        $("#brush-option").show();
    }, function () {
        $("#brush-option").hide();
    }
);

// eraser
var eraserPopper = new Popper($("#eraser"),$("#eraser-option"),{
    placement : "buttom"
});
$("#eraser").hover(function(){
        if(stroke.type==="eraser"){
            $("#eraser-option").show();
        }
    },function(){
        $("#eraser-option").hide();
});
$("#eraser-option").hover(function () {
        $("#eraser-option").show();
    }, function () {
        $("#eraser-option").hide();
    }
);
// text
var textPopper = new Popper($("#text"),$("#text-option"),{
    placement : "buttom"
});
$("#text").hover(function(){
        if(stroke.type==="text"){
            $("#text-option").show();
        }
    },function(){
        $("#text-option").hide();
});
$("#text-option").hover(function () {
        $("#text-option").show();
    }, function () {
        $("#text-option").hide();
    }
);

// picture
var picturePopper = new Popper($("#picture"),$("#picture-option"),{
    placement : "buttom"
});
$("#picture").hover(function(){
        if(stroke.type==="picture"){
            $("#picture-option").show();
        }
    },function(){
        $("#picture-option").hide();
});
$("#picture-option").hover(function () {
        $("#picture-option").show();
    }, function () {
        $("#picture-option").hide();
    }
);

// ============== click to draw ===========
$("canvas").mousedown(function(event){
    var classname = stroke.type[0].toLocaleUpperCase() + stroke.type.slice(1,stroke.type.length);
    var newShapeCode = "var shape = new " + classname + "('" + stroke.type + "');";
    eval(newShapeCode);
    display.stack.push(shape);
    shape.down(event);
    $("canvas").mousemove(function (event) { 
        shape.move(event);
    });
    $("canvas").mouseup(function () {
        shape.up(event); 
        $("canvas").off("mousemove");
        $("canvas").mousemove(function(event){
            cursor.move(event);        
        });
    });
});
$("canvas").mousemove(function(event){
    cursor.move(event);
});

// ============= click to clean ========
$("#delete").click(function(){
    var cursor = display.stack[0];
    display.stack = [];
    display.stack.push(cursor);
    display.undoStack = [];
});

$("#undo").click(function(){
    if(display.stack.length>=2){
        var shape = display.stack.pop();
        display.undoStack.push(shape);
    }
});
$("#redo").click(function(){
    if(display.undoStack.length>=1){
        var shape = display.undoStack.pop();
        display.stack.push(shape);
    }
});
function downloadImg(){
    var download = document.getElementById("download-link");
    var image = document.getElementById("canvas").toDataURL("image/png")
                .replace("image/png", "image/octet-stream");
          download.setAttribute("href", image);
};