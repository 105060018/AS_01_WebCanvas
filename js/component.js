var stroke = {
    type : "brush",
    color : "#000000",
    width : 3,
    update : function(){
        var size = 3;
        if(this.type==="brush" || this.type==="eraser"){
            var selector = "#" + this.type + "-size";
            size = $(selector).slider("value");
        }
        this.width = size;
    }
};
var display = {
    ctx : document.getElementById('canvas').getContext('2d'),
    stack : [],
    undoStack : []
};
class Shape {
    constructor(type) {
        this.type = type;
        this.text = "";
    }
    show(){

    }
    down(event){

    }
    move(event){

    }
    up(event){

    }
}
class Brush extends Shape{
    constructor(){
        super("brush");
        this.path = [];
        this.color = stroke.color;
        this.width = stroke.width;
    }
    show(){
        display.ctx.beginPath();
        display.ctx.moveTo(this.path[0].x,this.path[0].y);
        this.path.forEach(function (position) { 
            display.ctx.lineTo(position.x,position.y);
        });
        display.ctx.strokeStyle = this.color;
        display.ctx.lineWidth = this.width;
        display.ctx.stroke();
    }
    down(event){
        this.path.push({
            x : event.clientX - $("canvas").position().left,
            y : event.clientY - $("canvas").position().top
        });
    }
    move(event){
        this.path.push({
            x : event.clientX - $("canvas").position().left,
            y : event.clientY - $("canvas").position().top
        });
    }
}
class Eraser extends Shape{
    constructor(){
        super("eraser");
        this.path = [];
        this.width = stroke.width;
    }
    show(){
        display.ctx.globalCompositeOperation = "destination-out";
        display.ctx.beginPath();
        display.ctx.moveTo(this.path[0].x,this.path[0].y);
        this.path.forEach(function (position) { 
            display.ctx.lineTo(position.x,position.y);
        });
        display.ctx.lineWidth = this.width;
        display.ctx.stroke();                
        display.ctx.globalCompositeOperation = "source-over";
    }
    down(event){
        this.path.push({
            x : event.clientX - $("canvas").position().left,
            y : event.clientY - $("canvas").position().top
        });
    }
    move(event){
        this.path.push({
            x : event.clientX - $("canvas").position().left,
            y : event.clientY - $("canvas").position().top
        });
    }
}
class Circle extends Shape{
    constructor(){
        super("circle");
        this.color = stroke.color;
        this.start = {
            x : 0,
            y : 0
        };
        this.end = {
            x : 0,
            y : 0
        };
        this.radius = 0;
    }
    show(){
        display.ctx.beginPath();
        display.ctx.fillStyle = this.color;
        display.ctx.arc(
            (this.start.x +this.end.x)/2,
            (this.start.y +this.end.y)/2,
            this.radius,
            0,Math.PI*2,1
        );
        display.ctx.fill();        
    }
    down(event){
        this.start = {
            x : event.clientX - $("canvas").position().left,
            y : event.clientY - $("canvas").position().top
        };
    }
    move(event){
        this.end = {
            x : event.clientX - $("canvas").position().left,
            y : event.clientY - $("canvas").position().top
        };
        var dx = this.start.x - this.end.x;
        var dy = this.start.y - this.end.y;
        this.radius = Math.sqrt( dx*dx + dy*dy)/2;
    }
}
class Rectangle extends Shape{
    constructor(){
        super("rectangle");
        this.color = stroke.color;
        this.start = {
            x : 0,
            y : 0
        };
        this.end = {
            x : 0,
            y : 0
        };
    }
    show(){
        display.ctx.fillStyle = this.color;
        display.ctx.fillRect(this.start.x,this.start.y,this.end.x - this.start.x,this.end.y - this.start.y);      
    }
    down(event){
        this.start = {
            x : event.clientX - $("canvas").position().left,
            y : event.clientY - $("canvas").position().top
        };
        this.end = {
            x : event.clientX - $("canvas").position().left,
            y : event.clientY - $("canvas").position().top
        };
    }
    move(event){
        this.end = {
            x : event.clientX - $("canvas").position().left,
            y : event.clientY - $("canvas").position().top
        };
    }
}
class Triangle extends Shape{
    constructor(){
        super("triangle");
        this.color = stroke.color;
        this.start = {
            x : 0,
            y : 0
        };
        this.end = {
            x : 0,
            y : 0
        };
    }
    show(){
        display.ctx.beginPath();    
        display.ctx.fillStyle = this.color;  
        display.ctx.moveTo((this.start.x+this.end.x)/2 , this.start.y);
        display.ctx.lineTo(this.start.x,this.end.y);
        display.ctx.lineTo(this.end.x,this.end.y);
        display.ctx.fill();
    }
    down(event){
        this.start = {
            x : event.clientX - $("canvas").position().left,
            y : event.clientY - $("canvas").position().top
        };
        this.end = {
            x : event.clientX - $("canvas").position().left,
            y : event.clientY - $("canvas").position().top
        };
    }
    move(event){
        this.end = {
            x : event.clientX - $("canvas").position().left,
            y : event.clientY - $("canvas").position().top
        };
    }
}
class Text extends Shape{
    constructor(){
        super("text");
        this.position = {
            x : 0,
            y : 0
        };
        this.color = stroke.color;
        this.text = "";
        this.fontFamily = "Arial";
        this.fontSize = 12;
    }
    show(){
        display.ctx.fillStyle = this.color; 
        display.ctx.font = this.fontSize + "pt " + this.fontFamily;
        display.ctx.fillText(this.text,this.position.x, this.position.y); 
    }
    down(event){
        this.position = {
            x : event.clientX - $("canvas").position().left,
            y : event.clientY - $("canvas").position().top
        };
        this.text = $(".toolbox .content").val();
        this.fontFamily = $(".toolbox .font-family").val();
        this.fontSize = $(".toolbox .font-size").val();
    }
    move(event){

    }
}
class Cursor extends Shape{
    constructor(){
        super("cursor");
        this.position = {
            x : 0,
            y : 0
        };
        this.type = stroke.type;
        this.enable = 0;
    }
    show(){
        if(this.enable){
            var img = new Image();
            img.src = "./src/cursor/" + this.type + ".svg";
            display.ctx.drawImage(img, this.position.x, this.position.y-30, 30, 30);
        }
    }
    display(){
        this.enable = 1;
    }
    hide(){
        this.enable = 0;
    }
    move(event){
        this.position = {
            x : event.clientX - $("canvas").position().left,
            y : event.clientY - $("canvas").position().top
        };
        this.type = stroke.type;
    }
}

class Picture extends Shape{
    constructor(){
        super("picture");
        this.src = "./default.svg";
        this.start = {
            x : 0,
            y : 0
        };
        this.end = {
            x : 0,
            y : 0
        };
    }
    show(){
        var img = new Image();
        img.src = this.src;
        display.ctx.drawImage(img, this.start.x,this.start.y,this.end.x - this.start.x,this.end.y - this.start.y);     
    }
    down(event){
        var input = document.getElementById("picture-src");
        var fReader = new FileReader();
        if(input.files[0]!==undefined){
            fReader.readAsDataURL(input.files[0]);
            var picture = this;
            fReader.onloadend = function(event){
                var img = document.getElementById("yourImgTag");
                picture.src = event.target.result;
            }
    
            this.start = {
                x : event.clientX - $("canvas").position().left,
                y : event.clientY - $("canvas").position().top
            };
            this.end = {
                x : event.clientX - $("canvas").position().left,
                y : event.clientY - $("canvas").position().top
            };
        }
    }
    move(event){
        this.end = {
            x : event.clientX - $("canvas").position().left,
            y : event.clientY - $("canvas").position().top
        };
    }
}